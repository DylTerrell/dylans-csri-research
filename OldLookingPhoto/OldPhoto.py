from PIL import Image, ImageFilter

img1= Image.open("blackSplash.jpg").resize((900, 600))
img2 = Image.open("oldPicBike.jpg").resize((900, 600))

img2 = img2.filter(ImageFilter.GaussianBlur(radius = 3))
img1 = img1.convert("RGBA")
img2 = img2.convert("RGBA")

r, g, b, alpha = img2.split()

alpha = alpha.point(lambda i: i>0 and 20)

composite_image = Image.composite(img1, img2, alpha)
composite_image.show()

