from PIL import Image 

#open image
sunflower = Image.open("Sunflower.jpg").resize((900, 600))
ocean = Image.open("Ocean.jpg").resize((900, 600))

#converts images to 'RGBA'
ocean = ocean.convert("RGBA")
sunflower = sunflower.convert("RGBA")

#stores color from an image into these variables 'r, g, b, alpha'
#alpha represents the transparency of an image
r, g, b, alpha = sunflower.split()

#changes the transparency of the sunflower image
#Ex: (lambda i: i > 0 and 0) = not transparent - note that this will overshadow an image
#Ex: (lambda i: i > 0 and 255) = transparent - note that this will overshadow an image
alpha = alpha.point(lambda i: i > 0 and 180) 

#changes the contrast of the image
#Ex: threshold = 0 --> low contrast
#Ex: threshold = 255 --> high contrast 
threshold = 180
sunflower = sunflower.point(lambda p: p > threshold and 255)

#blends both images together 
composite_image = Image.composite(sunflower, ocean, alpha)

#display image
composite_image.show()
