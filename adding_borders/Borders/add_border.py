from PIL import Image, ImageOps

sunflower = Image.open("Chicago.jpg")

#add border to image
image_with_border = ImageOps.expand(sunflower.copy(), border = 20, fill = 'black')
image_with_border.show()
