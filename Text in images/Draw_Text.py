from PIL import Image, ImageDraw, ImageFont, ImageFilter

image = Image.open("Sunflower.jpg")
image_blur = image.filter(ImageFilter.CONTOUR)
draw = ImageDraw.Draw(image_blur)
font = ImageFont.truetype('arial.ttf', 72)
fontType = ImageFont.truetype('arial.ttf', 18)
draw.text(xy=(350,50), text = 'Sunflower', fill = (200,100,20), font = font)
draw.text(xy=(800,600), text = '@Dylan Terrell', fill= (200,100,20), font = fontType)
image_blur.save("text_sunflower.jpg")
image_blur.show()
