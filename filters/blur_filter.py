from PIL import Image, ImageFilter

sunflower = Image.open("Sunflower.jpg")

'''
other filters that can be used:
BLUR, CONTOUR, DETAIL, EDGE_ENHANCE, EDGE_ENHANCE_MORE, EMBOSS
FIND_EDGES, SMOOTH, SMOOTH_MORE, AND SHARPEN
'''

#filters (image effects)
sunflower_filter = sunflower.filter(ImageFilter.EMBOSS)
sunflower_filter.show()