
from PIL import Image
from PIL import ImageFilter

def main():
    original_photo = Image.open( "Sunflower.jpg" )
    original_photo.show()

    (width, height) = original_photo.size
    print( "width = ", width )
    print( "height = ", height )

    # Place the pre-defined filters of the ImageFilter
    # class in a list.

    filters = []

    # filter 0
    filters.append( ImageFilter.BLUR )

    # filter 1
    filters.append( ImageFilter.CONTOUR )

    # filter 2
    filters.append( ImageFilter.DETAIL )

    # filter 3
    filters.append( ImageFilter.EDGE_ENHANCE )

    # filter 4
    filters.append( ImageFilter.EDGE_ENHANCE_MORE )

    # filter 5
    filters.append( ImageFilter.EMBOSS )

    # filter 6 
    filters.append( ImageFilter.FIND_EDGES )

    # filter 7
    filters.append( ImageFilter.SHARPEN )

    # filter 8
    filters.append( ImageFilter.SMOOTH )

    # filter 9
    filters.append( ImageFilter.SMOOTH_MORE )

    # Select filter number 0--9
    changed_photo = original_photo.filter( filters[2] )

    changed_photo.show()

if __name__ == "__main__":
    main()
