from PIL import Image

#opens image
img = Image.open("Sunflower.jpg")

#changes the contrast of the sunflower
#Tip: change threshold to get different colors of the sunflower
threshold = 150
img = img.point(lambda p: p > threshold and 255)
img.show()