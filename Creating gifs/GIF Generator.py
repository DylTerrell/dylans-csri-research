'''
Make sure to install imageio by going to your command prompt
and type pip install imageio imageio-ffmpeg
'''
import imageio
import os 

#Create path to mp4 video  
clip = os.path.abspath("cells_splitting.mp4")

'''
This method takes the clip (input_path) and format of the clip (target_format), which 
we can change to AVI or .mp4, and changes the clip to a gif 
'''
def gif_maker(input_path, target_format):
	'''
	output_path splits the input_path ("cells_splitting.mp4")
	into ('cells','_','splitting','.mp4') the gif format ('cells','_','splitting','.gif')
	'''
	output_path = os.path.splitext(input_path)[0] + target_format

	#converts input_path to output_path
	print(f'converting {input_path} \n to {output_path}')

	#imageio reads the clip
	reader = imageio.get_reader(input_path)

	#set the fps of the video to the fps of the gif
	fps = reader.get_meta_data()['fps']

	#creates gif and sets the fps that we made previously
	writer = imageio.get_writer(output_path, fps = fps)

	#extracts the frames in the clip 
	for frames in reader:
		#append frames to gif 
		writer.append_data(frames)
		print(f'Frame {frames}')

	print('Done!')
	writer.close()

#call function 
gif_maker(clip, '.gif')
#End of program
