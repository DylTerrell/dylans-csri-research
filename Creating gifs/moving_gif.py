from PIL import Image, ImageDraw, ImageFont

font = ImageFont.truetype("arial", 36)
def create_image_with_text(size, text):
    image = Image.new('RGB', (600, 50), "yellow")
    draw = ImageDraw.Draw(image)
    draw.text((size[0], size[1]), text, font = font, fill = "black")
    return image

frames = []
x, y = 0, 0
for i in range(100):
    new_frame = create_image_with_text((x - 100, y), "HELLO")
    frames.append(new_frame)
    x += 4
    y += 1

frames[0].save("moving_gif", format = 'GIF', append_images = frames[1:], save_all = True, duration = 30, loop = 0)