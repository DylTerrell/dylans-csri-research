from PIL import Image, ImageFont, ImageDraw, ImageFilter

x = 0
inc = 10
def create_image_with_text(size, text):
    global x, inc
    img = Image.open("Creating gifs/Colored Pencils.jpg")
    img = img.filter(ImageFilter.GaussianBlur(5))
    w,h = img.size
    draw = ImageDraw.Draw(img)
    draw.text((w // 5, h // 2), text, font = fnt, fill=(x,x,x))
    x += inc
    return img

# Create the frames
frames = []
"""
Function appends the new frames and iterates text
one word at a time
"""
def roll(text):
    global x
    for i in range(len(text)+1):
        new_frame = create_image_with_text((0,0), text[:i])
        frames.append(new_frame)
    x = 0

fnt = ImageFont.truetype("arial", 36)
all_text = """ HEY EVERYONE
LOOK AT WHAT I MADE A GIF
THAT CHANGES COLORS
AND ACTUALLY DISPLAYS WORDS!!!
--------------------------
Continue to keep up
the great work and go beyond!!!""".splitlines()
for text in all_text:
    roll(text)


# Save into a GIF file that loops forever
frames[0].save('background2.gif', format='GIF',
               append_images=frames[1:], save_all=True, duration=80, loop=0)
print("Done")
frames[0].show('background2.gif', format='GIF',
               append_images=frames[1:], save_all=True, duration=80, loop=0)
