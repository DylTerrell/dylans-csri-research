from PIL import Image, ImageDraw
'''
In the method below we create a new RGB image using Image.new that has
a specified size and background color

We then use ImageDraw to draw a red ellipse (circle) into our image using 
the coordinates given in the arguments
'''
def create_image_with_ball(width, height, ball_x, ball_y, ball_size):
	img = Image.new('RGB', (width, height), (0,255,255))
	draw  = ImageDraw.Draw(img)

	#draw.ellipse takes a 4-tuple(x_0, y0, x1, y1) where (x0, y0) is the
	#top-left bound of the box
	# and (x1, y1) is the bottom-right bound of the box

	draw.ellipse((ball_x, ball_y, ball_x + ball_size, ball_y + ball_size), fill = 'red')
	return img



#Now we just create frames while moving the ball down
#Create the frames
frames = []
x, y = 0, 0
for i in range(20):
    new_frame = create_image_with_ball(1000, 1000, x, y, 40)
    frames.append(new_frame)
    x += 50
    y += 50

# Save into a GIF file that loops forever
frames[0].save('moving_ball.gif', format='GIF', append_images=frames[1:], save_all=True,
duration=100, loop=0)
#The GIF image will loop forever (loop=0, if loop was set to 1 then it will loop 1 time,
#2 = 2 times, etc...)
