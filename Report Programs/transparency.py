from PIL import Image 

sunflower = Image.open("Sunflower.jpg").resize((900, 600))
ocean = Image.open("Ocean.jpg").resize((900, 600))

#ocean = ocean.convert("RGBA")
#sunflower = sunflower.convert("RGBA")

#r, g, b, alpha = ocean.split()
r, b, alpha = sunflower.split()

threshold = 0
sunflower = sunflower.point(lambda p: p > threshold and 255)

composite_image = Image.composite(sunflower, ocean, alpha)
composite_image.show()