import math
from PIL import Image


def compute_shade_arithmetic_mean( color ):
    (red, green, blue) = color

    average = (red + green + blue) // 3

    return (average, average, average)
# compute_shade_arithmetic_mean()

def compute_shade_min_max_mean( color ):
    (red, green, blue) = color

    minimum = min( red, green, blue )
    maximum = max( red, green, blue )

    average = (minimum + maximum) // 2

    return (average, average, average)
# end of computer_shade_min_max_mean()

def compute_shade_weighted_average( color, weights ):
    (red, green, blue) = color
    (red_weight, green_weight, blue_weight) = weights

    average = int(red_weight * red + green_weight * green + blue_weight * blue)

    return (average, average, average)
# end of compute_shade_weighted_average()

def choose_color( color, i, j, width, height ):
    # TO-DO: experiment with other formulas
    # for deciding which pixels should be
    # drawn in color and which should be
    # drawn in black & white

    # TO-DO experiment with each of the
    # following color-to-share-of-gray algorithms

    rule_width = 4
    white = (255, 255, 255)

    if abs(i - (3 * width // 4)) < rule_width:
        return white
    elif abs( i - (width // 2)) < rule_width:
        return white
    elif abs( i - (width // 4)) < rule_width:
        return white

    elif i >  (3 * width // 4):
        # convert color to shade of gray by
        # using the arithmetic mean algorithm
        shade = compute_shade_arithmetic_mean( color )
        return shade

    elif i > width // 2:
        # convert color to shade of gray by
        # using the mean of the minimum and maximum
        # of the colors 3 components
        shade = compute_shade_min_max_mean( color )
        return shade

    elif i > width // 4:
        # convert color to shade of gray by
        # using the weighted average algorithm
        weights = (0.30, 0.59, 0.11)
        shade = compute_shade_weighted_average( color, weights )
        return shade

    else:
        # do not convert color to a shade of gray
        return color
# end of choose_color()

def color_to_black_and_white( photo ):
    (width, height) = photo.size

    copy = photo.copy()

    for i in range( width ):
        for j in range( height ):
            color = photo.getpixel( (i, j) )

            color = choose_color( color, i, j, width, height )

            copy.putpixel( (i, j), color )

    return copy
# end of color_to_black_and_white()


def main():
    photo = Image.open( "Sunflower.jpg" )
    #photo.show()

    (width, height) = photo.size
    print( "width = ", width )
    print( "height = ", height )

    bw_photo = color_to_black_and_white( photo )

    bw_photo.show()

if __name__ == "__main__":
    main()
