import math
from PIL import Image, ImageDraw, ImageFilter, ImageOps

class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def id(self):
        # multiplier to shift decimal point 4 spaces to the right
        ten_thousand = 10000

        prefix = int( round( ten_thousand * self.x ) )
        suffix = int( round( ten_thousand * self.y ) )
        return ten_thousand * prefix + suffix

    def map(self, bounds):
        min_x = bounds.upper_left.x
        min_y = bounds.lower_right.y

        max_x = bounds.lower_right.x
        max_y = bounds.upper_left.y

        width = max_x - min_x
        height = max_y - min_y

        x = min_x + width * self.x
        y = min_y + height * self.y

        return (x, y)

class Rectangle:
    def __init__(self, upper_left, lower_right):
        self.upper_left = upper_left
        self.lower_right = lower_right

    def in_bounds(self, point):
        min_x = self.upper_left.x
        min_y = self.lower_right.y

        max_x = self.lower_right.x
        max_y = self.upper_left.y

        x = point.x
        y = point.y

        x_in_bounds = min_x <= x <= max_x
        y_in_bounds = min_y <= y <= max_y

        return x_in_bounds and y_in_bounds

class Hexagon:
    def __init__(self, center, radius, phase = 0):
        self.center = center
        self.outer_radius = radius
        self.inner_radius = radius * math.cos( math.pi/6 )
        self.phase = phase
        self.vertices = []
        self.shared_vertex_neighbors = []
        self.shared_edge_neighbors = []

        # compute the coordinates of the hexagon's vertices

        vertices = []
        for n in range( 0, 6 ):
            angle = n * math.pi / 3
            x = center.x + radius * math.cos (angle + phase )
            y = center.y + radius * math.sin (angle + phase )
            vertices.append( Point(x,y) )
        self.vertices = vertices

        # compute coordinates of centers of hexagons that
        # share a vertex with this hexagon
        shared_vertex_neighbors = []
        angle = phase
        for n in range( 0, 6 ):
            x = center.x + 2.0 * self.outer_radius * math.cos( angle )
            y = center.y + 2.0 * self.outer_radius * math.sin( angle )
            p = Point(x, y)
            shared_vertex_neighbors.append( p )
            angle += math.pi / 3
        self.shared_vertex_neighbors = shared_vertex_neighbors

        # compute coordinates of centers of hexagons that
        # share an edge with this hexagon
        shared_edge_neighbors = []
        angle = phase + math.pi / 6
        for n in range( 0, 6 ):
            x = center.x + 2.0 * self.inner_radius * math.cos( angle )
            y = center.y + 2.0 * self.inner_radius * math.sin( angle )
            p = Point(x, y)
            shared_edge_neighbors.append( p )
            angle += math.pi / 3
        self.shared_edge_neighbors = shared_edge_neighbors

    def circumscribed_circle(self, bounds):
        # compute coordinates of smallest square that
        # encloses the hexagon

        ulx = self.center.x - self.outer_radius
        uly = self.center.y + self.outer_radius

        lrx = self.center.x + self.outer_radius
        lry = self.center.y - self.outer_radius

        upper_left = Point( ulx, uly)
        lower_right = Point( lrx, lry)

        upper_left = upper_left.map( bounds )
        lower_right = lower_right.map( bounds )

        (ulx, uly) = upper_left
        (lrx, lry) = lower_right

        ulx = int(ulx)
        uly = int(uly)

        lrx = int(lrx)
        lry = int(lry)

        return ((ulx, lry), (lrx, uly))

    def inscribed_circle(self, bounds):
        ulx = self.center.x - self.inner_radius
        uly = self.center.y + self.inner_radius

        lrx = self.center.x + self.inner_radius
        lry = self.center.y - self.inner_radius

        upper_left = Point( ulx, uly)
        lower_right = Point( lrx, lry)

        upper_left = upper_left.map( bounds )
        lower_right = lower_right.map( bounds )

        (ulx, uly) = upper_left
        (lrx, lry) = lower_right

        ulx = int(ulx)
        uly = int(uly)

        lrx = int(lrx)
        lry = int(lry)

        return ((ulx, lry), (lrx, uly))

    def map_vertices(self, bounds):
        vertices = []
        for v in self.vertices:
            vertices.append( v.map(bounds) )
        return vertices

class Tiling:
    def __init__(self, radius, phase, bounds):
        self.radius = radius
        self.phase = phase
        self.bounds = bounds

    def new_and_in_bounds(self, tile_center, centers):
        is_new = not tile_center.id() in centers
        is_in_bounds = self.bounds.in_bounds( tile_center )

        return is_new and is_in_bounds

    def tiling(self):
        tile_center = Point( 0.5, 0.5 )
        candidates = [ tile_center ]
        centers = set()
        tiles = []

        while candidates:

            tile_center = candidates.pop()

            if self.new_and_in_bounds( tile_center, centers ):
                new_tile = Hexagon(tile_center, self.radius, self.phase)

                candidates.extend( new_tile.shared_edge_neighbors )

                centers.add( tile_center.id() )
                tiles.append( new_tile )

        return tiles

def make_image_square( image ):
    (width, height) = image.size

    # crop to get a square image
    if width > height:
        margin = (width - height) // 2
        ulx = margin
        uly = 0
        lrx = width - margin
        lry = height
        image = image.crop( (ulx, uly, lrx, lry) )
    else:
        margin = (height - width) // 2
        ulx = 0
        uly = margin
        lrx = width
        lry = height - margin
        image = image.crop( (ulx, uly, lrx, lry) )

    (width, height) = image.size

    image = ImageOps.fit( image, (width // 2, height // 2) )

    return image

def main():
    radius = 1 / 32
    phase = 0.0

    photo = Image.open('Sunflower.jpg')
    photo = make_image_square( photo )

    (width, height) = photo.size

    #white_square = Image.new( mode = 'RGB', size = (width, height), color  =(255, 255, 255) )
    black_square = Image.new( mode = 'RGB', size = (width, height), color  =(0, 0, 0) )
    #mask = white_square.copy()

    photo = photo.filter( ImageFilter.GaussianBlur( radius * width ) )


    bounds = Rectangle( Point(0, height), Point(width, 0) )

    canvas = ImageDraw.Draw( black_square )

    unit_square = Rectangle( Point(0.0, 1.0), Point(1.0, 0.0) )

    tiling = Tiling( radius, phase, unit_square )
    tiles = tiling.tiling()
    print( type(tiles) )

    for t in tiles:
        (column, row) = t.center.map( bounds )
        column = int( column )
        row = int( row )
        color = photo.getpixel( (column, row) )

        vertices = t.map_vertices(bounds)
        canvas.polygon( vertices, fill = color, outline = (0,0,0) )

        #canvas.ellipse( t.inscribed_circle(bounds), fill = color, outline = (0, 0, 0) )

    black_square.show()
    photo.close()

if __name__ == '__main__':
    main()
