from PIL import Image, ImageEnhance

img = Image.open("Sunflower.jpg")
img.show()

img1 = ImageEnhance.Contrast(img)
contrast = 2.5
image_contrasted = img1.enhance(contrast)
image_contrasted.show()