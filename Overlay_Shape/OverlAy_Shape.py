from PIL import Image, ImageDraw, ImageOps

#Open image
sunflower = Image.open("Sunflower.jpg")
print(sunflower.size)

#overlay shape onto image
image_with_shape = sunflower.copy()

#coordinates (upper-left, bottom-right)
ImageDraw.Draw(image_with_shape).rectangle([(325, 157), (638, 460)])
image_with_shape.show()