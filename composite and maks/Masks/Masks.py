from PIL import Image, ImageDraw, ImageFilter

im1 = Image.open("Sunflower.jpg")
im2 = Image.open("Ocean.jpg").resize(im1.size)
mask = Image.new('L', im1.size, 0)
draw = ImageDraw.Draw(mask)
draw.ellipse((320, 130, 650, 470), fill = 255)
mask_blur = mask.filter(ImageFilter.GaussianBlur(10))
im = Image.composite(im1, im2, mask_blur)
im.save("Mask_composite.jpg")
im.show()