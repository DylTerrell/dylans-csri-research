from PIL import Image 

#opens both images and resizes them to the same size
#both pictures have to be same size and mode in order to blend together 
ocean = Image.open("Ocean.jpg").resize((900, 600))
sailboat = Image.open("Sailboat.jpg").resize((900, 600))

ocean = ocean.convert("RGBA")
sailboat = sailboat.convert("RGBA")

#takes colors from sailboat 
r, g, b, alpha = sailboat.split()

'''
the point() method can be used to translate the pixel values of an image.
Each pixel is processed according to that function. 
'''
alpha = alpha.point(lambda i: i>0 and 90)

#use Image.composite() module to blend images
composite_image = Image.composite(ocean, sailboat, alpha)

#assigns the aspect ratio to the output image 
composite_image.thumbnail((900, 600))

#display image
composite_image.show()


